//1) символ або ключове слово, яке використовується для встановлення логічних зв'язків між виразами чи умовами в програмуванні та логіці
//2) &&-and,||-or,!-not 

// 1)
var age = prompt("Введіть свій вік:");
age = parseInt(age);
if (isNaN(age)) {
    alert("Введіть коректне число для віку.");
} else if (age < 12) {
    alert("Ви є дитиною.");
} else if (age < 18) {
    alert("Ви є підлітком.");
} else {
    alert("Ви є дорослим.");
}

// 2)
var month = prompt("Будь ласка, введіть назву місяця");
var daysInMonth;
switch (month) {
    case "січень":
    case "березень":
    case "травень":
    case "липень":
    case "серпень":
    case "жовтень":
    case "грудень":
        daysInMonth = 31;
        break;
    case "квітень":
    case "червень":
    case "вересень":
    case "листопад":
        daysInMonth = 30;
        break;
    case "лютий":
        var yearString = prompt("Будь ласка, введіть рік:");
        var year = parseInt(yearString);
        if (isNaN(year)) {
            alert("Будь ласка, введіть коректне число для року.");
        } else {
            daysInMonth = (year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0)) ? 29 : 28;
        }
        break;
    default:
        alert("Введено невірний місяць.");
}
if (daysInMonth !== undefined) {
    console.log(`У місяці ${month} ${daysInMonth} днів.`);
}

